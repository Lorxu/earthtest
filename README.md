# EarthTest
EarthTest is a Minetest game focused on realism, simplicity, and coherence. It's meant to feel like one game, not a collection of parts, and be realistic without getting too complicated.

I plan on redoing every single texture (most of them are ok, but could be a lot better, and many are awful.) EarthTest aims for a minimalist style, looking somewhat soft while still being 16x16. Not too much going on.

## Installation
Download or clone the repository, and extract it to .minetest/games. See https://wiki.minetest.net/Games#Installing_games.

## Contributing
Feel free to work on this project any way you'd like. You can suggest things in Issues if you have any.

If you don't want to, or don't feel like, programming, you can work on the textures (this is probably going to be the most work), or suggest your ideas in the Issues tab. (It's not just for bugs, suggestions are welcome too!)

## License
Most of this isn't mine. Original licenses are included in the mods. Any modifications I've made are WTFPL.