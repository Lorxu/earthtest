local modpath = minetest.get_modpath(minetest.get_current_modname()) 

crafting = {}
crafting.append_to_formspec = nil -- nil leaves this with default background, "bgcolor[#080808BB;true]background[5,5;1,1;gui_formbg.png;true]listcolors[#00000069;#5A5A5A;#141318;#30434C;#FFF]".

dofile(modpath .. "/config.lua")
dofile(modpath .. "/table.lua")
if not minetest.get_modpath("pipeworks") then
	dofile(modpath .. "/furnace.lua")
end
if crafting.config.enable_autotable then
	dofile(modpath .. "/autotable.lua")
end

simplecrafting_lib.set_crafting_guide_def("table", {
	append_to_formspec = crafting.append_to_formspec,
})

if crafting.config.import_default_recipes then

	simplecrafting_lib.register_recipe_import_filter(function(legacy_method, legacy_recipe)
		
		if legacy_method == "normal" then
			return "table", crafting.config.clear_default_crafting
		elseif legacy_method == "cooking" then
			legacy_recipe.fuel_grade = {}
			legacy_recipe.fuel_grade.min = 0
			legacy_recipe.fuel_grade.max = math.huge
			return "furnace", false--crafting.config.clear_default_crafting -- Don't clear furnace recipes
		elseif legacy_method == "fuel" then
			legacy_recipe.grade = 1
			return "fuel", false--crafting.config.clear_default_crafting
		end
	end)

	simplecrafting_lib.import_legacy_recipes()
end

if crafting.config.clear_default_crafting then
	-- If we've cleared all native crafting recipes, add the table back
	-- in to the native crafting system so that the player can
	-- build that and access everything else through it
	simplecrafting_lib.minetest_register_craft({
		output = "crafting:table",
		recipe = {
			{"group:tree","",""},
			{"","",""},
			{"","",""},
		},
	})
end
